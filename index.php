<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>YUR SAYUUUR</title>
		<!-- FAVICON -->
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" >
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Kalam:400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Just+Another+Hand" rel="stylesheet">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="css/animate.min.css">
		<!-- FontAwesome CSS -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- Slick Slider CSS -->
		<link rel="stylesheet" href="slick/slick.css">
		<!-- Reset CSS -->
		<link rel="stylesheet" href="css/reset.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="css/responsive.css">
	    <!-- The Farm House colors. We have chosen the color Vermilion for this default
	          page. However, you can choose any other color by changing color css file.
	    -->
	    <link rel="stylesheet" type="text/css" href="css/colors/default-vermilion.css">
	    <!-- <link rel="stylesheet" type="text/css" href="css/colors/shamrock.css"> -->
	    <!-- <link rel="stylesheet" type="text/css" href="css/colors/keylimepie.css"> -->
	    <!-- <link rel="stylesheet" type="text/css" href="css/colors/scooter.css"> -->
	    <!-- <link rel="stylesheet" type="text/css" href="css/colors/goldengrass.css"> -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="js">
	    <!-- Page loader -->
	    <div id="preloader"></div>
		<!-- header area start -->
		<header class="farm-navbar-area">
			<nav class="navbar navbar-expand-sm">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggler collapsed" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1" aria-expanded="false">&#x2630;</button>
						<a class="navbar-brand"
						href="#home">
							<img src="img/logotricode.jpeg" alt="" width="80" height="80">
						
						</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav menu navbar-nav ml-auto">
							<li class="current-menu-item nav-item"><a href="#home" class="nav-link">Home</a>
							</li>
							<li class="nav-item"><a href="#about" class="nav-link">Tentang</a>
							</li>
							<li class="nav-item"><a href="#product" class="nav-link">Produk</a>
							</li>
							<li class="nav-item"><a href="#prices" class="nav-link">Harga</a>
							</li>
							<li class="nav-item"><a href="#gallery" class="nav-link">Galeri</a>
							</li>
							<li class="nav-item"><a href="#faq" class="nav-link">FAQ</a>
							</li>
							<li class="nav-item"><a href="#testimonials" class="nav-link">Review</a>
							</li>
							<li class="nav-item"><a href="#app" class="nav-link">App</a>
							</li>
							<li class="nav-item"><a href="#contact" class="nav-link"><span class="fa fa-pencil-square-o"></span></a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header><!-- header area end -->
		<!-- slider area start -->
		<section class="farm-welcome-text wow fadeIn" id="home">
		   <div class="farm-home-slider-area">
			   <div class="farm-home-slider">
				   <div class="farm-slider-item">
					   <img src="img/home-slider/slideke1.jpg" alt="">
					   <div class="item-content">
							<div class="container">
								<div class="row">
									<div class="col-12 text-center">
										<div class="welcome-text-content">
											<div class="weltext">
												<h2 class="welcome-text"><span>Produk</span>Segar</h2>
												<h2 class="welcome-text">langsung diambil dari petani.</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
					   </div>
				   </div>
				   <div class="farm-slider-item">
					   <img src="img/home-slider/slideke2.jpg" alt="">
					   <div class="item-content">
							<div class="container">
								<div class="row">
									<div class="col-12 text-center">
										<div class="welcome-text-content">
											<div class="weltext">
												<h2 class="welcome-text">Kemasan <span>Terjaga</span></h2>
												<h2 class="welcome-text">Dikemas dengan sangat rapih sehingga sayuran masih segar</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
					   </div>
				   </div>
				   <div class="farm-slider-item">
					   <img src="img/home-slider/slideke3.jpg" alt="">
					   <div class="item-content">
							<div class="container">
								<div class="row">
									<div class="col-12 text-center">
										<div class="welcome-text-content">
											<div class="weltext">
												<h2 class="welcome-text">Harga <span>Update</span></h2>
												<h2 class="welcome-text">Harga produk kami selalu terupdate.</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
					   </div>
				   </div>
				   <div class="farm-slider-item">
					   <img src="img/home-slider/slideke4.jpg" alt="">
					   <div class="item-content">
							<div class="container">
								<div class="row">
									<div class="col-12 text-center">
										<div class="welcome-text-content">
											<div class="weltext">
												<h2 class="welcome-text">Tidak perlu ke<span>PASAR</span></h2>
												<h2 class="welcome-text">Tidak usah pergi ke pasar, cukup order melalui aplikasi.</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
					   </div>
				   </div>
			   </div>
		   </div>
			<div class="farm-social-icon">
				<a href="http://facebook.com" class="fa fa-facebook"></a>
	            <a href="http://twitter.com" class="fa fa-twitter"></a>
	            <a href="http://youtube.com" class="fa fa-youtube"></a>
			</div>
		</section><!-- slider area end -->
		<!-- about area start -->
		<section id="about" class="farm-about-us">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="farm-about-content">
							<h2 class="content-title">Tentang Kami</h2>
							<h3 class="wow zoomIn" style="text-align:justify;background-color:grey;padding:20px;color:white;border-radius:20px;opacity:0.9;">YUR SAYUUUR adalah sebuah aplikasi berbasis mobile dan bisa berjalan di sistem operasi Android dan IOS. YUR SAYUUUR di kembangkan oleh CV TRICODE dari Bandung. YUR SAYUUUR dapat membantu kita khususnya ibu ibu yang sebelumnya sering pergi ke pasar untuk belanja dan memakan waktu dan tenaga, dengan YUR SAYUUUR kini lebih mudah belanja tidak perlu ke pasar. Cukup order melalui aplikasi. </h3>
							<h4 class="wow zoomIn"><span>1</span>Order > jam 12 siang akan di proses esok harinya</h4>
							<h4 class="wow zoomIn"><span>2</span>Gratis plastik untuk bungkus barang </h4>
							<h4 class="wow zoomIn"><span>3</span>Pembayaran Mudah dan Praktis (Transfe Bank, Gopay, OVO dll) </h4>
							<h4 class="wow zoomIn"><span>4</span>Harga selalu update setiap harinya </h4>
							<div class="contact-botton text-center wow zoomIn">
								<a href="#">Hubungi Kami</a>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="farm-about-img wow zoomIn">
							<img src="img/logotricode.jpeg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section><!-- about area end -->
		<!-- product area start -->
		<section id="product" class="farm-latest-products">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="farm-section-title">
							<h2 class="section-title">Produk Terbaru</h2>
							<h4>Langsung diambil dari petani dan masih segar.</h4>
						</div>
					</div>
				</div>
				<div class="farm-product-slider">
					<div class="row product-select">
						<div class="col-md-6">
							<div class="farm-single-product">
								<div class="single-product">
									<img src="img/latest-products/broccoli.png" alt="" width="220" height="220">
								</div>
								<div class="product-free">
									<img src="img/latest-products/pro-icon-1.png" alt="">
								</div>
								<div class="hover-product"><a href="#">See Product</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="farm-single-product">
								<div class="single-product">
									<img src="img/latest-products/potato.png" alt="" width="220" height="220">
								</div>
								<div class="product-free">
									<img src="img/latest-products/pro-icon-2.png" alt="">
								</div>
								<div class="hover-product"><a href="#">See Product</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="farm-single-product">
								<div class="single-product">
									<img src="img/latest-products/celery.png" alt="" width="220" height="220">
								</div>
								<div class="product-free">
									<img src="img/latest-products/pro-icon-3.png" alt="">
								</div>
								<div class="hover-product"><a href="#">See Product</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="farm-single-product">
								<div class="single-product">
									<img src="img/latest-products/terong.png" alt="" width="220" height="220">
								</div>
								<div class="product-free">
									<img src="img/latest-products/pro-icon-4.png" alt="">
								</div>
								<div class="hover-product"><a href="#">See Product</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row product-select">
						<div class="col-md-6">
							<div class="farm-single-product">
								<div class="single-product">
									<img src="img/latest-products/tomat.png" alt="" width="220" height="220">
								</div>
								<div class="product-free">
									<img src="img/latest-products/pro-icon-1.png" alt="">
								</div>
								<div class="hover-product"><a href="#">See Product</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="farm-single-product">
								<div class="single-product">
									<img src="img/latest-products/jahe.png" alt="" width="220" height="220">
								</div>
								<div class="product-free">
									<img src="img/latest-products/pro-icon-2.png" alt="">
								</div>
								<div class="hover-product"><a href="#">See Product</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="farm-single-product">
								<div class="single-product">
									<img src="img/latest-products/cabai.png" alt="" width="220" height="220">
								</div>
								<div class="product-free">
									<img src="img/latest-products/pro-icon-3.png" alt="">
								</div>
								<div class="hover-product"><a href="#">See Product</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="farm-single-product">
								<div class="single-product">
									<img src="img/latest-products/bawangputih.png" alt="" width="220" height="220">
								</div>
								<div class="product-free">
									<img src="img/latest-products/pro-icon-4.png" alt="">
								</div>
								<div class="hover-product"><a href="#">See Product</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- product area end -->
		<!-- prices area start -->
		<section id="prices" class="farm-pricing-list">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="farm-pricing-table wow zoomIn">
							<table>
								<thead>
									<tr class="table-heading">
										<th>Produk</th>
										<th>Harga</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Cabai</td>
										<td>Rp. 35.000/kg</td>
									</tr>
									<tr>
										<td>Bawang Putih</td>
										<td>Rp. 20.000/kg</td>
									</tr>
									<tr>
										<td>Tomat</td>
										<td>Rp. 10.000/kg</td>
									</tr>
									<tr>
										<td>Jahe</td>
										<td>Rp. 9.000/kg</td>
									</tr>
									<tr>
										<td>Kentang</td>
										<td>Rp. 12.000/kg</td>
									</tr>
									<tr>
										<td>Brokoli</td>
										<td>Rp. 20.000/kg</td>
									</tr>
									<tr>
										<td>Terong</td>
										<td>Rp. 13.000/kg</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="mx-md-auto col-lg-7 col-md-6">
						<div class="farm-price-content">
								<h2 class="content-title">Daftar Harga</h2>
								<h4>Harga dapat berubah ubah dan akan selalu update setiap harinya</h4>
							<div class="contact-botton text-left wow zoomIn">	<a href="#">Hubungi Kami</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- prices area end -->
		<!-- portfolio area start -->
		<section id="gallery" class="farm-portfolio-section">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="farm-section-title">
							<h2 class="section-title">Galeri Produk</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="farm-project-nav">
						<ul>
							<li class="active" data-filter="*">Semua</li>
							<li data-filter=".vegetables">Sayuran</li>
							<li data-filter=".fruits">Buah</li>
							<li data-filter=".discount">Diskon</li>
						</ul>
					</div>
				</div>
				<div class="farm-project-active">
					<div class="farm-single-project fruits">
						<div class="project-img">
							<img src="img/product-gallery/tomat.png" alt="theiran.com" width="200" height="200">
							<div class="project-weight">
								<h3>Rp. 10.000/kg</h3>
							</div>
						</div>
						<h4>Tomat</h4>
					</div>
					<div class="farm-single-project vegetables">
						<div class="project-img">
							<img src="img/product-gallery/cabai.png" alt="theiran.com" width="200" height="200">
							<div class="project-weight">
								<h3>Rp. 35.000/kg</h3>
							</div>
						</div>
						<h4>Cabai</h4>
					</div>
					<div class="farm-single-project discount">
						<div class="project-img">
							<img src="img/product-gallery/broccoli.png" alt="theiran.com" width="200" height="200">
							<div class="project-weight">
								<h3>Rp. 20.000/kg</h3>
							</div>
						</div>
						<h4>Brokoli</h4>
					</div>
					<div class="farm-single-project discount">
						<div class="project-img">
							<img src="img/product-gallery/bawangputih.png" alt="theiran.com" width="200" height="200">
							<div class="project-weight">
								<h3>Rp. 20.000/kg</h3>
							</div>
						</div>
						<h4>Bawang Putih</h4>
					</div>
					<div class="farm-single-project vegetables">
						<div class="project-img">
							<img src="img/product-gallery/potato.png" alt="theiran.com" width="200" height="200">
							<div class="project-weight">
								<h3>Rp. 12.000/kg</h3>
							</div>
						</div>
						<h4>Kentang</h4>
					</div>
					<div class="farm-single-project vegetables">
						<div class="project-img">
							<img src="img/product-gallery/terong.png" alt="theiran.com" width="200" height="200">
							<div class="project-weight">
								<h3>Rp. 13.000/kg</h3>
							</div>
						</div>
						<h4>Terong</h4>
					</div>
					<div class="farm-single-project discount">
						<div class="project-img">
							<img src="img/product-gallery/jahe.png" alt="theiran.com" width="200" height="200">
							<div class="project-weight">
								<h3>Rp. 9.000/kg</h3>
							</div>
						</div>
						<h4>Jahe</h4>
					</div>
					<div class="farm-single-project">
						<div class="project-img">
							<img src="img/product-gallery/bawangmerah.png" alt="theiran.com" width="200" height="200">
							<div class="project-weight">
								<h3>Rp. 26.000/kg</h3>
							</div>
						</div>
						<h4>Bawang Merah</h4>
					</div>
				</div>
			</div>
		</section><!-- portfolio area end -->
		<!-- faq area start -->
		<section id="faq" class="farm-faqs-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="farm-faqs-title">
							<h2 class="content-title">FAQ</h2>
						</div>
						<div class="farm-house-accourdion">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel">
									<div class="card-header" role="tab" id="headingOne">
										<h4 class="card-title">
											  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												Produk apa saja yang dijual?
											  </a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse show" role="tabpanel"
									aria-labelledby="headingOne">
										<div class="card-body">
											<p>Semua jenis sayuran dan buahan tersedia di aplikasi kami</p>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="card-header" role="tab" id="headingTwo">
										<h4 class="card-title">
											  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Bagaiaman cara pembayaran?</a>
										</h4>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
									aria-labelledby="headingTwo">
										<div class="card-body">
											<p>Pembayaran dapat dilakukan melalui Trasnfer Bank (Virtual Account), Gopay, OVO, Alfamart, Indomaret dll</p>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="card-header" role="tab" id="headingThree">
										<h4 class="card-title">
											  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Pengiriman barangnya bagaimana?</a>
										</h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
									aria-labelledby="headingThree">
										<div class="card-body">
											<p>Pengiriman Barang dilakukan oleh kurir kami sehingga lebih cepat dan tentunya lebih murah ongkos kirimnya.</p>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="card-header" role="tab" id="headingFour">
										<h4 class="card-title">
											 <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">Bagaimana jika barang yang sudah datang ternyata tidak fresh?</a>
										</h4>
									</div>
									<div id="collapsefour" class="panel-collapse collapse" role="tabpanel"
									aria-labelledby="headingFour">
										<div class="card-body">
											<p>Jika barang yang dibeli sudah tidak lagi fresh (karena kesalahan kami) silahkan bawa ke kantor kami untuk dilakukan retur barang</p>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="card-header" role="tab" id="headingFive">
										<h4 class="card-title">
											  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">Berapa banyak barang yang bisa dibeli diini?</a>
										</h4>
									</div>
									<div id="collapsefive" class="panel-collapse collapse" role="tabpanel"
									aria-labelledby="headingFive">
										<div class="card-body">
											<p>Barang dapat dibeli dengan jumlah tak terbatas</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="tometo-man wow zoomIn">
							<img src="img/faq.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section><!-- faq area end -->
		<!-- testimonial top area start -->
		<section id="testimonials" class="farm-client-says-section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="bloc">&#x200B;&#x200C;<span>&#x201C;</span>
						</div>
						<div class="single-client-btn">	<a href="#" class="client-btn">Review Pelanggan</a>
						</div>
					</div>
				</div>
				<div class="row blocquate-slick">
					<div class="col-12 col-md-11 text-right">
						<div class="single-client">
							<h2>&quot;Sayuran saya sangat bersih dan segar, tidak kotor.&quot;</h2>
							<h4>- Asep Bedog</h4>
						</div>
					</div>
					<div class="col-12 col-md-11 text-right">
						<div class="single-client">
							<h2>&quot;Dengan aplikasi ini saya tidak repot lagi ke pasar tinggal order di aplikasi, pesanan langsung datang.&quot;</h2>
							<h4>- Mamat Panci</h4>
						</div>
					</div>
				</div>
			</div>
		</section><!-- testimonial top area end -->

		<!-- app area start -->
		<section id="app" class="farm-android-mokup">
			<div class="container RTC">
				<div class="row">
					<div class="col-md-6 text-left app-content">
						<div class="get-app">
							<div class="farm-section-title text-left">
								<h2 class="section-title">YUR SAYUUUR</h2>
							</div>
						</div>
						<h4>YUR SAYUUUR sudah tersedia di berbagai palform! Unduh Sekarang:</h4>
						<div class="app-button text-left">
							<a href="#"><img src="img/app-store.png" alt=""></a>
							<a href="#"><img src="img/google-play.png" alt=""></a>
						</div>
					</div>
					<div class="app-img">
						<img src="img/app-mobile-view.png" alt="">
					</div>
				</div>
			</div>
		</section><!-- app area end -->
		<!-- contact area start -->
		<section id="contact" class="farm-contact-section">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="farm-section-title">
								<h2 class="section-title">Kontak Kami</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<form id="contact-form">
							<div class="farm-contact-form">
								<!-- IF MAIL SENT SUCCESSFULLY -->
								<div class="success">Pesan anda telah terkirim.</div>
								<!-- IF MAIL SENDING UNSUCCESSFULL
								-->
								<div class="error">E-mail harus valid.</div>
								<div class="single-inputc">
									<input type="text" id="name" name="name" placeholder="Nama*" required="required">
								</div>
								<div class="single-inputc">
									<input type="email" id="email_address" name="email_address" placeholder="Email*"
									required="required">
								</div>
								<div class="single-inputc">
									<input type="text" id="phone_number" name="phone_number" placeholder="Nomor Telepon">
								</div>
								<div class="porpose">
									<i class="fa fa-angle-down" aria-hidden="true"></i>
									<select class="porpose-select"
									id="contact_reason" name="contact_reason">
										<option value="Purpose of the message">Pengiriman</option>
										<option value="Purpose of the message">Kualitas Produk</option>
										<option value="Purpose of the message">Ketersediaa Produk</option>
										<option value="Purpose of the message">Harga Produk</option>
									</select>
								</div>
								<div class="text-area">
									<textarea id="message" name="message" placeholder="Pesan ..."></textarea>
								</div>
								<div class="single-submit">
									<input type="submit" name="submit" id="submit" value="Hubungi Kami">
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-6">
						<div class="single-contact">
							<div class="contact-icon">
								<i class="fa fa-map-marker"></i>
							</div>
							<div class="contact-content">
								<h3>Lokasi Kami</h3>
								<p>Jalan Raya Cicalengka no 9999</p>
							</div>
						</div>
						<div class="single-contact">
							<div class="contact-icon">
								<i class="fa fa-phone"></i>
							</div>
							<div class="contact-content">
								<h3>Nomor Telepon</h3>
								<p>08983792832</p>
							</div>
						</div>
						<div class="single-contact">
							<div class="contact-icon">
								<i class="fa fa-envelope-o"></i>
							</div>
							<div class="contact-content">
								<h3>Email</h3>
								<p>admin@tricode.com</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- app area end -->
		<!-- footer area start -->
		<footer class="copyright-section">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<p>&copy; 2021 YUR SAYUUUR <span class="fa fa-heart"></span> by <a href="https://tricode.com">TRICODE</a> </p>
					</div>
				</div>
			</div>
		</footer><!-- footer area end -->
		<!-- scrolltotop start -->
		<div>
			<a href="#" class="scrollToTop text-center" >
				<i class="scroll-fa fa fa-angle-up" aria-hidden="true"></i>
			</a>
		</div><!-- scrolltotop end -->
		<!-- jQuery min JS -->
		<script src="js/jquery.min.js"></script>
		<!-- Popper JS -->
		<script src="js/popper.min.js"></script>
		<!-- jQuery Easing JS -->
		<script src="js/jquery.easing.1.3.js"></script>
		<!-- Bootstra JS -->
		<script src="js/bootstrap.min.js"></script>
		<!-- jQuery Nav JS -->
		<script src="js/jquery.nav.js"></script>
		<!-- jQuery Sticky JS -->
		<script src="js/jquery.sticky.js"></script>
		<!-- jQuery Isotop JS -->
		<script src="js/isotope.pkgd.min.js"></script>
		<!-- jQuery Slick JS -->
		<script src="slick/slick.min.js"></script>
		<!-- Parallax JS -->
		<script src="js/parallax.min.js"></script>
		<!-- WOW JS active -->
		<script src="js/wow-1.3.0.min.js"></script>
		<!-- jQuery active JS -->
		<script src="js/main.js"></script>
	</body>
</html>